#ifndef  __APP_MQTT_H
#define __APP_MQTT_H

typedef void (*mqtt_handler_t)( esp_mqtt_client_handle_t);
typedef void (*mqtt_data_handler_t)(char* ,char* );
void mqtt_app_start(void);
void mqtt_client_subscribe(void *cb);
void mqtt_even_data(void *cb);
// void mqtt_client_subscribe();
// void json_gen_test(json_gen_test_result_t *result, char *key1, bool value1, char *key2, int value2, char *key3, char *value3);
#endif