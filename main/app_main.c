/* MQTT Mutual Authentication Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"


#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"

#include "wifi_iot.h"
#include "http_server_app.h"
#include "app_mqtt.h"
#include "output_iot.h"
#include "input_iot.h"
#include "app_nvs.h"
#include "json_generator.h"
#include "json_parser.h"
#include "app_ota.h"
static const char *TAG = "EXAMPLE";
#define led_pin 2
#define button_pin 0
#define KEY     "data"
#define KEY2    "string"
typedef struct {
    char buf[256];
    size_t offset;
} json_gen_test_result_t;

json_gen_test_result_t *resul;
// #define json_test_str	"{\n\"str_val\" :    \"JSON Parser\", \n" 
// 			"\t\"float_val\" : 2.0,\n" 
// 			"\"int_val\" : 2017,\n" 
// 			"\"bool_val\" : false,\n" 
// 			"\"supported_el\" :\t [\"bool\",\"int\","
// 			"\"float\",\"str\"" 
// 			",\"object\",\"array\"],\n" 
// 			"\"features\" : { \"objects\":true, "
// 			"\"arrays\":\"yes\"},\n"
// 			"\"int_64\":109174583252}"
char json_test_str[]="{\"str_val\":\"JSON Parser\",\"float_val\":2.0,\"int_val\":2017,\"bool_val\":false,\
\"supported_el\":[ \"bool\",\"int\",\"float\",\"str\", \"object\", \"array\", \
\"features\":{ \"objects\":true,\"arrays\":\"yes\"}, \"int_64\":109174583252 }";


static esp_err_t wifi_info_handler(httpd_req_t *req)
{
    char buf[100];
    httpd_req_recv(req, buf,MIN(req->content_len, sizeof(buf)));
    //"Deviot2@12345679@"
    char ssid[30] = "";
    char pass[30] = "";
    printf("->data: %s\n", buf);
    char *pt = strtok(buf, "|");
    if(pt)
        strcpy(ssid, pt);
    pt = strtok(NULL, "|");
    if(pt)    
        strcpy(pass, pt);

    printf("->ssid: %s\n", ssid);
    printf("->pass: %s\n", pass);


    wifi_disconnect();
    wifi_init_sta(ssid,pass);
    return ESP_OK;
}
    
static esp_err_t led_status_handler(httpd_req_t *req)
{
     char resp_str[100];

    sprintf(resp_str,"{\"ledStatus\": \"%d\"}",gpio_get_level(led_pin));
    httpd_resp_send(req,(const char*) resp_str, strlen(resp_str));
    // ,DHT11_read().temperature,DHT11_read().humidity
    // printf("%d\n",gpio_get_level(led_pin));
    return ESP_OK;
}

void led_interrupt()
{
    output_io_toggle(led_pin);
}

static void flush_str(char *buf, void *priv)
{
    json_gen_test_result_t *result = (json_gen_test_result_t *)priv;
    if (result) {
        if (strlen(buf) > sizeof(result->buf) - result->offset) {
            printf("Result Buffer too small\r\n");
            return;
        }
        memcpy(result->buf + result->offset, buf, strlen(buf));
        result->offset += strlen(buf);
    }
}
void json_gen_test(json_gen_test_result_t *result, char *key1, bool value1, char*key2, int value2, char*key3, char *value3)
{
	char buf[20];
    memset(result, 0, sizeof(json_gen_test_result_t));
	json_gen_str_t jstr;
	json_gen_str_start(&jstr, buf, sizeof(buf), flush_str, result);    
    json_gen_start_object(&jstr);
    json_gen_obj_set_bool(&jstr, key1, value1);
    json_gen_obj_set_int(&jstr, key2, value2);
    json_gen_obj_set_string(&jstr, key3, value3);
    json_gen_end_object(&jstr);
    json_gen_str_end(&jstr);
    printf("Result: %s\n", result->buf);
}
void add_client_mqtt(esp_mqtt_client_handle_t client)
{
    esp_mqtt_client_subscribe(client, "/dylan/setLed", 0);
    esp_mqtt_client_subscribe(client, "/dylan/ota", 0);
}
void data_led_handler( char *topic,char *data)
{
    printf("topic=%s\n",topic);
    printf("data=%s\n",data);

    if (strcmp(topic,"/dylan/setLed")==0)
    {
        if (strstr(data,"LED ON")) output_io_set_level(2,1);
        else if (strstr(data,"LED OFF")) output_io_set_level(2,0);
    // printf("%s\n",data_led);
    }
    if (strcmp(topic,"/dylan/ota")==0)
    app_ota_start(data);//"http://192.168.1.24/app_ota.bin"

}
void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    output_io_create(led_pin);
    input_io_create(button_pin,HI_TO_LO);
    input_set_callback(led_interrupt);
    output_io_set_level(led_pin,0);

    wifi_init();
     int value=0;
    app_nvs_get_value(KEY, &value);
    // printf("value=%d\n");
    value++;
    app_nvs_set_value(KEY,value);
    char mang_set[50];
    sprintf(mang_set,"hello %d\n",value);
    char mang[50];
    app_nvs_get_str(KEY2,mang);
    printf("%s",mang);
    app_nvs_set_string(KEY2,mang_set);

    app_config(PROVISION_ACCESSPOINT);
    // wifi_init_sta("HUONG LUA","19931993");

    start_webserver("/");
    http_add("/wifiinfo",HTTP_POST,wifi_info_handler);
    http_add("/ledData",HTTP_GET,led_status_handler);
    mqtt_app_start();
    mqtt_client_subscribe(add_client_mqtt);
    mqtt_even_data(data_led_handler);

    // json_gen_test_result_t resul;
    // json_gen_test(&resul,"kt",true, "id", 123, "type", "dictionary");
    // printf("data json: %s\n",resul.buf);
    // jparse_ctx_t jctx;
	// int ret = json_parse_start(&jctx, json_test_str, strlen(json_test_str));
	// if (ret != OS_SUCCESS) 
    // {
	// 	printf("Parser failed\n");
	// 	// return -1;
	// }
	// char str_val[64];
	// int int_val, num_elem;
	// int64_t int64_val;
	// bool bool_val;
	// float float_val;
    // printf("%s\n",json_test_str);
	// if (json_obj_get_string(&jctx, "str_val", str_val, sizeof(str_val)) == OS_SUCCESS)
	// 	printf("str_val %s\n", str_val);
	// if (json_obj_get_float(&jctx, "float_val", &float_val) == OS_SUCCESS)
	// 	printf("float_val %f\n", float_val);
	// if (json_obj_get_int(&jctx, "int_val", &int_val) == OS_SUCCESS)
	// 	printf("int_val %d\n", int_val);
	// if (json_obj_get_bool(&jctx, "bool_val", &bool_val) == OS_SUCCESS)
	// 	printf("bool_val %s\n", bool_val ? "true" : "false");
	// if (json_obj_get_array(&jctx, "supported_el", &num_elem) == OS_SUCCESS) {
	// 	printf("Array has %d elements\n", num_elem);
	// 	int i;
	// 	for (i = 0; i < num_elem; i++) {
	// 		json_arr_get_string(&jctx, i, str_val, sizeof(str_val));
	// 		printf("index %d: %s\n", i, str_val);
	// 	}
	// 	json_obj_leave_array(&jctx);
	// }
	// if (json_obj_get_object(&jctx, "features") == OS_SUCCESS) {
	// 	printf("Found object\n");
	// 	if (json_obj_get_bool(&jctx, "objects", &bool_val) == OS_SUCCESS)
	// 		printf("objects %s\n", bool_val ? "true" : "false");
	// 	if (json_obj_get_string(&jctx, "arrays", str_val, sizeof(str_val)) == OS_SUCCESS)
	// 		printf("arrays %s\n", str_val);
	// 	json_obj_leave_object(&jctx);
	// }
	// if (json_obj_get_int64(&jctx, "int_64", &int64_val) == OS_SUCCESS)
	// 	printf("int64_val %lld\n", int64_val);
	// json_parse_end(&jctx);

}
