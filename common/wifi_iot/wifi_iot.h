#ifndef  __WIFI_IOT_H
#define __WIFI_IOT_H
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"

typedef enum{
    PROVISION_ACCESSPOINT = 0,
    PROVISION_SMARTCONFIG = 1
}   provision_type_t;
void wifi_init(void);
int wifi_init_sta(char *wifi_ssid,char *wifi_pass);
void wifi_init_softap(char* AP_ssid,char* AP_pass);
void app_config(provision_type_t provisition_type);
void wifi_disconnect(void);
#endif