#include <esp_wifi.h>
#include "http_server_app.h"
static httpd_handle_t server = NULL;
static const char *TAG = "example";

extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");

static uri_handle_t hello_get_handler(httpd_req_t *req)
{

    httpd_resp_set_type(req,"text/html");
    httpd_resp_send(req,(const char *) index_html_start, index_html_end-index_html_start);
    return ESP_OK;
}

void http_add(const char *url,httpd_method_t method,uri_handle_t *callback)
{
    const httpd_uri_t post = {
            .uri       = url,
            .method    = method,
            .handler   = callback,
            .user_ctx  = NULL
                };
    httpd_register_uri_handler(server, &post);

}

esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    if (strcmp("/dht11", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/hello URI is not available");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    } else if (strcmp("/echo", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/echo URI is not available");
        /* Return ESP_FAIL to close underlying socket */
        return ESP_FAIL;
    }
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Some 404 error message");
    return ESP_FAIL;
}

void start_webserver(const char *url)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        http_add(url,HTTP_GET,hello_get_handler);
        // httpd_register_err_handler(server, HTTPD_404_NOT_FOUND, http_404_error_handler);
    }
    else
    ESP_LOGI(TAG, "Error starting server!");

}

void stop_webserver(void)
{
    // Stop the httpd server
    httpd_stop(server);
}




